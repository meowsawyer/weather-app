//
//  WeatherInfo.swift
//  WeatherApp
//
//  Created by Nick Frai on 05.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import UIKit

struct WeatherInfo {
    
    let infoString: String
    
    init(weatherObject: WeatherObjectResponse) {
        let roundedMinTemp = (weatherObject.weatherDetails.minTemp - 273.15).rounded()
        let roundedMaxTemp = (weatherObject.weatherDetails.maxTemp - 273.15).rounded()

        self.infoString = "Today. Minimum temperature is \(String(format: "%.0f", roundedMinTemp))°C, max temperature is \(String(format: "%.0f", roundedMaxTemp))°C. \(weatherObject.weather[0].description)."
    }
}

