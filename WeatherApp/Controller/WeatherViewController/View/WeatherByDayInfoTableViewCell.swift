//
//  WeatherByDayInfoTableViewCell.swift
//  WeatherApp
//
//  Created by Nick Frai on 05.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import UIKit

class WeatherByDayInfoTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet private weak var weatherImageView: UIImageView!
    @IBOutlet private weak var minTemperatureLabel: UILabel!
    @IBOutlet private weak var maxTemperatureLabel: UILabel!
    
    var data: WeatherByHour? {
        didSet {
            weatherImageView.image = nil
            updateData()
        }
    }
    
    private func updateData() {
        guard let data = data else { return }
        
        dayLabel.text = data.date.weekday
        weatherImageView.image = data.state.image
        minTemperatureLabel.text = String(format: "%.0f", data.dailyMinTemp)
        maxTemperatureLabel.text = String(format: "%.0f", data.dailyMaxTemp)
    }
}



