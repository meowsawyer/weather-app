//
//  WeatherInfoTableViewCell.swift
//  WeatherApp
//
//  Created by Nick Frai on 05.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import UIKit

class WeatherInfoTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var infoLabel: UILabel!
    
    var data: WeatherInfo? {
        didSet {
            updateData()
        }
    }
    
    private func updateData() {
        guard let data = data else { return }
        
        infoLabel.text = data.infoString
    }
}


