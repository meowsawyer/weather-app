//
//  Date+Extensions.swift
//  WeatherApp
//
//  Created by meowsawyer on 08/01/2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

extension Date {
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    
    var hour: Int {
           return Calendar.current.component(.hour, from: self)
       }
    
    var weekday: String {
        let df = DateFormatter()
        return df.weekdaySymbols[Calendar.current.component(.weekday, from: self) - 1]
    }
    
    var time: String {
        let df = DateFormatter()
        df.dateFormat = "HH:mm"
        return df.string(from: self)
    }
}
