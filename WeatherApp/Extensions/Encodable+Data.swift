//
//  Encodable+Data.swift
//  WeatherApp
//
//  Created by meow on 08.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

extension Encodable {
    var data: Data? {
        let data = try? encoder.encode(self)
        return data
    }
}

private extension Encodable {
    var encoder: JSONEncoder {
        return JSONEncoder()
    }
}
