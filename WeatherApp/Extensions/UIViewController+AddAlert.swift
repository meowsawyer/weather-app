//
//  UIViewController+AddAlert.swift
//  WeatherApp
//
//  Created by meow on 07.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import UIKit

extension UIViewController {
    func addAlert(title: String? = nil, message: String? = nil, actions: [UIAlertAction]? = nil) {
        var actionsArray = [UIAlertAction]()
        
        if let newActions = actions {
            actionsArray = newActions
        } else {
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            actionsArray.append(okAction)
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actionsArray {
            alert.addAction(action)
        }
        present(alert, animated: true)
    }
}
