//
//  IpResponse.swift
//  WeatherApp
//
//  Created by meow on 07.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

struct IpResponse: Decodable {
    
    var city: String
}
