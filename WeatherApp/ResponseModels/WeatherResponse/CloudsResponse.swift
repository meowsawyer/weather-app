//
//  Clouds.swift
//  WeatherApp
//
//  Created by Nick Frai on 04.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

struct CloudsResponse: Codable {
    
    let cloudiness: Double
    
    private enum CodingKeys: String, CodingKey {
        case cloudiness = "all"
    }
}
