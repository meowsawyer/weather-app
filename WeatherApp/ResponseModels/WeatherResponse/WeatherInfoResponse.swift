//
//  Weather.swift
//  WeatherApp
//
//  Created by Nick Frai on 04.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

struct WeatherInfoResponse: Codable {
    
    let state: String
    let description: String
    
    private enum CodingKeys: String, CodingKey {
        case state = "main"
        case description
    }
}
