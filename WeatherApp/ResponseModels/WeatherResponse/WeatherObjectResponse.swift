//
//  WeatherObject.swift
//  WeatherApp
//
//  Created by Nick Frai on 04.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

struct WeatherObjectResponse: Codable {
    
    let timestamp: Double
    let weather: [WeatherInfoResponse]
    let weatherDetails: WeatherDetailsResponse
    let clouds: CloudsResponse?
    let wind: WindResponse?
    
    private enum CodingKeys: String, CodingKey {
        case timestamp = "dt"
        case weather
        case weatherDetails = "main"
        case clouds
        case wind
    }
}
