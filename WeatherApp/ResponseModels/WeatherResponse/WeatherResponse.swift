//
//  WeatherResponse.swift
//  WeatherApp
//
//  Created by meowsawyer on 04/01/2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

struct WeatherResponse: Codable {
    
    let weatherObjects: [WeatherObjectResponse]
    let city: CityResponse
    
    private enum CodingKeys: String, CodingKey {
        case weatherObjects = "list"
        case city
    }
}
