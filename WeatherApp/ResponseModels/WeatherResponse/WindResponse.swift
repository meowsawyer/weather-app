//
//  Wind.swift
//  WeatherApp
//
//  Created by Nick Frai on 04.01.2020.
//  Copyright © 2020 meowsawyer. All rights reserved.
//

import Foundation

struct WindResponse: Codable {
    
    let speed: Double
}

